from modules import atap
from globals import messages as msg


def send_timetable(update, context):
    try:
        route = atap.atap.find_route(context.args[0].upper())
        url = route['timetable_url']
        chat_id = update.message.chat_id
        context.bot.send_document(chat_id=chat_id, document=url)
    # General exception because the bot has to send the same error in every case
    except Exception:
        update.message.reply_text(msg.error_wrong_route)
        update.message.reply_text(msg.help_query_route)
