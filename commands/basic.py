from globals import messages as msg


def start(update, context):
    """ Send the welcome message """
    update.message.reply_text(msg.welcome)
