welcome = "Ciao! Sono il bot non ufficiale dell'ATAP di Pordenone! " \
          "\n\n🚍 Utilizzi 🚍" \
          "\nOttenere gli orari di tutte le linee dell ATAP in maniera facile e veloce utilizzando un solo comando" \
          "\n\n🚍 Istruzioni 🚍" \
          "\nUsa il comando /orario per ottenere l'orario di una determinata linea"

error_wrong_route = "La linea che hai inserito non è stata trovata " \

help_query_route = "Per cercare una linea bisogna utilizzare il comando \"/orario <codice_linea>\"." \
                    "\nUn esempio di codice della linea è il numero riportato sul display della corriera " \
                    "Esempio: " \
                    "\tScrivere \"4\" per fare riferimento alla linea 4. " \
                    "\tIn certi casi la linea differisce per Andata e Ritorno perciò si può avere una lettera " \
                    "che segue il codice (A o R). Nel caso della 18 bisogna distinguere tra 18A e 18R"
