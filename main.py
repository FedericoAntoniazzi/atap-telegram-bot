from commands.basic import start
from commands.timetables import send_timetable

import sys

from telegram.ext import (
    Updater,
    CommandHandler
)

BOT_TOKEN = sys.argv[1]

updater = Updater(token=BOT_TOKEN, use_context=True)

dispatcher = updater.dispatcher


# Basic commands
dispatcher.add_handler(CommandHandler('start', start))


# Timetables
dispatcher.add_handler(CommandHandler('orario', send_timetable))

# Start the process
updater.start_polling()
updater.idle()
