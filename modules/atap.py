from .dbmanager import DatabaseManager
import modules.scraper as scraper
import os


class Atap:

    def __init__(self, db_name=None):
        if db_name is None:
            raise ValueError('Database name cannot be None')
        self.route_types = scraper.get_route_types()
        self.database = DatabaseManager(db_name)
        self.update_db()

    def update_db(self):
        self.database.reset()
        routes = scraper.routes()
        for route in routes:
            self.database.insert_route(route)

    def get_routes(self, is_urban=None):
        return self.database.get_routes(is_urban)

    def find_route(self, route):
        r = self.database.find_route(route)[0]
        return self._format_routes_dict(r)

    def _format_routes_dict(self, route):
        return {
            'full_name': route[1],
            'route_code': route[2],
            'path_brief': route[3],
            'timetable_url': route[4],
            'is_urban': route[5]
        }


atap = Atap(db_name="atap.sqlite")
