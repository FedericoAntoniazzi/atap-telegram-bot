from bs4 import BeautifulSoup
import requests

timetable_url = "https://www.atap.pn.it/orari-e-tariffe/linee-e-orari.html"
root_url = "https://www.atap.pn.it"


def get_route_types():
    route_types = dict()
    page = requests.get(timetable_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    title_class = "sppb-title-heading"

    types = list()
    for title in soup.find_all('h2', {'class': title_class}):
        types.append(title.text)

    route_types['urban_route'] = types[0].lower()
    route_types['extra_urban_route'] = types[1].lower()
    return route_types


def routes():
    div_container_class = "sppb-row-container"
    div_routes_class = "sppb-addon sppb-addon-module"
    title_class = "sppb-title-heading"

    page = requests.get(timetable_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    route_types = get_route_types()
    all_routes = list()

    for container in soup.find_all('div', {'class': div_container_class}):
        # Get the title of the section ("Linee urbane" or "Linee extraurbane")
        route_type = container.find('h2', {'class': title_class}).text.lower()
        routes_container = container.find('div', {'class': div_routes_class})

        for a in routes_container.find_all('a'):
            try:
                # Get the name of the route without extra characters
                route_name = a.text.strip()
                route_link = root_url + a['href']
            except Exception as e:
                continue

            all_routes.append(_format_route_dict(route_type, route_name, route_link, route_types))

    return all_routes


def _format_route_dict(route_type, route_name, route_link, route_types):
    def is_urban():
        return route_type == route_types['urban_route']

    def route_code():
        if route_type == route_types['urban_route']:
            return route_name.split()[1]
        return route_name.split()[0].split('_')[1]

    def short_path():
        if is_urban():
            return " ".join(route_name.split()[2:])
        return " ".join(route_name.split()[1:])

    result = {
        'full_name': route_name,
        'route_code': route_code().upper(),
        'timetable_url': route_link,
        'path_brief': short_path(),
        'is_urban': is_urban()
    }
    return result
