import sqlite3


class Database:

    def __init__(self, name=None):
        self.connection = None
        self.cursor = None

        if name:
            self.db_name = name
        else:
            raise ValueError("The field name can't be None, please write a name")

    def _connect(self):
        try:
            self.connection = sqlite3.connect(self.db_name)
            self.cursor = self.connection.cursor()
        except sqlite3.Error as e:
            print(e)

    def _close(self):
        self.connection = None
        self.cursor = None

    def execute(self, sql_instruction, *param):
        self._connect()
        self.cursor.execute(sql_instruction, *param)
        self.connection.commit()
        self._close()

    def query(self, sql_instruction, *param):
        self._connect()
        self.cursor.execute(sql_instruction, *param)
        rows = self.cursor.fetchall()
        self._close()
        return rows


class DatabaseManager:
    _create_routes_table = "CREATE TABLE IF NOT EXISTS routes ( " \
                           "id integer PRIMARY KEY," \
                           "full_name text NOT NULL," \
                           "route_code text NOT NULL," \
                           "path_brief text NOT NULL," \
                           "timetable_url text NOT NULL," \
                           "is_urban bit" \
                           ");"

    _add_route = "INSERT INTO routes(route_code, full_name, path_brief, timetable_url, is_urban)" \
                 "VALUES(?,?,?,?,?)"

    def __init__(self, db_name):
        self.db = Database(name=db_name)
        self.db.execute(self._create_routes_table)

    def insert_route(self, route):
        if not type(route) is dict:
            print(type(route))
            raise ValueError("Route must be a dictionary")

        self.db.execute(self._add_route, (route.get('route_code', None), route.get('full_name', None),
                                          route.get('path_brief', None), route.get('timetable_url', None),
                                          route.get('is_urban', False)))

    def get_routes(self, is_urban=None):
        if is_urban is None:
            # Returns all routes
            return self.db.query("SELECT * FROM routes")
        else:
            # Returns only urban or extraurban routes
            return self.db.query("SELECT * FROM routes WHERE is_urban = ?", (is_urban,))

    def _format_routes_dict(self, route):
        return {
                'full_name': route[1],
                'route_code': route[2],
                'timetable_url': route[3],
                'path_brief': route[4],
                'is_urban': route[5]
            }

    def find_route(self, route):
        return self.db.query("SELECT * FROM routes WHERE route_code = ?", (route,))

    def reset(self):
        # Drop the routes table
        self.db.execute("DROP TABLE routes")
        # Create the table again
        self.db.execute(self._create_routes_table)
